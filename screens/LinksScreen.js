import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'

class List extends Component {
  static navigationOptions = {
    title: 'Lista de recordatorios',
  };
   state = {
      names: [
         {
            id: 0,
            name: "" 
         },
      ]
   }
   alertItemName = (item) => {
      alert("recordatorio de "+this.props.navigation.state.params.medicine+" dentro de: "+this.props.navigation.state.params.datetime)
   }
   
   render() {
      return (
        <View> 
          {
            this.state.names.map((item, index) => (
              <TouchableOpacity
                  key = {item.id}
                  style = {styles.container}
                  onPress = {() => this.alertItemName(item)}>
                  
                  <Text style = {styles.text}>
                    {item.name}
                  </Text>
              </TouchableOpacity>
            ))
          }
        </View>
      )
   }
}
export default List

const styles = StyleSheet.create ({
   container: {
      padding: 10,
      marginTop: 3,
      backgroundColor: 'white',
      alignItems: 'center',
   },
   text: {
      color: '#000'
   }
})  