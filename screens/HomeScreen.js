import React from 'react';
import {Alert, AppRegistry, TextInput } from 'react-native';
import { Button } from 'react-native';
import DatePicker from 'react-native-datepicker'

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Recordatorio de medicamento',
  };
  constructor(props) {
    super(props);
    this.state = {date:"",
                  text:""  
                }
  }
  render() {
    return (
      <View style={{padding: 10}}>
        <TextInput
          style={{height: 40}}
          placeholder="Ingrese medicamento a recordar"
          onChangeText={(text) => this.setState({text})}
        />
        <DatePicker
          style={{width: 200}}
          date={this.state.datetime1}
          mode="datetime"
          format="DD-MM-YYYY HH:mm"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          minuteInterval={10}
          onDateChange={(datetime) => {this.setState({datetime1: datetime});}}
        />
        <Button
          onPress={() => {
            if(this.state.text != "" && this.state.datetime1 != undefined){
              this.props.navigation.navigate('Links',{ medicine: this.state.text, datetime: this.state.datetime1});
              Alert.alert('Se ha guardado');
            }else{
              Alert.alert('Debe seleccionar una fecha e ingresar');
            }
          }}
          title="Guardar"
        />
      </View>    
    );
  }
}